# Identification of clinical biomarkers in population plasma NMR metabolomics data 
* * *

Current common practice in clinical biomarker research is that a targeted assay is developed for an small molecule, 
substance or protein based on interest of the researcher, input from a research field or commercial interest, 
usually to aid in making diagnoses or to guide treatment. Examples are total cholesterol, C-reactive protein and NT-proBNP. 
After that, research is performed to investigate to which clinical phenotypes, diseases and prospective outcomes the biomarker relates.  

Nuclear Magnetic Resonance (NMR) spectroscopy is a technique gaining interest in clinical practice because it allows for quick, 
accurate and reliable assessment of many small molecules and detailed information on lipoproteins other total cholesterol for making diagnoses and guidance 
of treatment. To this end, companies like LabCorp in the United States of America have developed and validated a large number of specific assays using 
this technique. The NMR spectra of biological samples – usually EDTA plasma – that are generated using this technique are stored on 
files and contain much more information that the information required for the specific assays that are derived using these data.

A pipeline was build to pre-process, post-process and quantify <sup>1</sup>H-NMR spectra from patients who underwent kidney transplantation. 
The quantified peak tables can be used with multivarite statistics to investigate whether peaks are associated with any of the available clinical characteristics.

[![python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)](https://www.python.org/)
[![Python: v3.8](https://img.shields.io/badge/python-v3.0-yellow)](https://www.python.org/download/releases/3.0/)
[![Python package: nmrglue](https://img.shields.io/badge/Python%20package-nmrglue-yellow)](https://www.nmrglue.com/)
[![Python package: scipy](https://img.shields.io/badge/Python%20package-scipy-yellow)](https://www.scipy.org/)

## Description
* * *

The pipeline was build using [python version 3.0](https://www.python.org/download/releases/3.0/), and various python packages. From those various packages, the 
packages [nmrglue](https://www.nmrglue.com/)<sup>[1]</sup> and [scipy](https://www.scipy.org/)<sup>[3]</sup> played the most important role. Nmrglue was used for most of the pre-procssing steps, 
and scipy played an important role in both pre- and post-processing of the data. 

The pipeline consists of one python script, nameley [nmr_spectral_processing.py](code/nmr_spectral_processing.py), which pre-processes, 
post-processes, quntifies and plots the data. 

The pre-processing started of with processing the free induction decay (FID) data with a complex Fourier transformation. 
Following the complex Fourier transform, the data was corrected for phase using a linear phase correction. 
After phase correction the imaginary unit of the complex numbers were deleted, and the spectra were smoothed by applying a Savitzky-Golay filter.

Post-processing of the data starts with dividing the spectra into bins. Number of bins were calculated by subtracting the
the minimum from the maximum value of the ppm scales and dividing it by a delta, which could be changed to affect the number of bins. The data 
was then divided into the defined number of bins. Next, the spectral intensities were summed up per bin for each sample.
This results in a dataframe where each row represents a sample, and each column represents a bin. Then, the clinical metadatawas loaded in 
and added to the dataframe which holds the bin per sample information. The metadata was coupled to the right sample by using a part of the sample name 
and the SampleID column which was present inside the metadata dataframe. From the dataframe containing the bin information a 
heatmap was produced which can be used to identify the difference in intensity across the samples.

Furthermore, peak picking was performed on the data. The height in intensity and location on the ppm scale of the peaks, 
as well as the left and right bases of the peaks were collected. Afterwards, the area under the curve (AUC) and the sum of 
the intensities from the left to the right bases of the peaks were calculated. This information was saved in .csv form and written out 
to a specified result directory. 

At last, statistical analysis was performed on the bin dataframe. Which included PCA, independent t-test, PLS-DA, and linear regression.

The pipeline consist of multiple packages, which are listed [here](#markdown-header-components).

## Data
* * *

During this project <sup>1</sup>H-NMR spectra data of blood samples from the Agilent/Varian spectrometer was used of 6800 human volunteers in a PREVEND study. 
Each sample contained six spectra. The blood was collected from the volunteers in K<sub>2</sub>EDTA tubes or serum tubes after overnight fast or non-fasting<sup>[9]</sup>. 
Either plasma or serum was separated using the specified centrifugation guidelines of the manufacturer, 
and kept in a refrigerator (4°C) up to five days before use, or frozen at -80 °C for later use<sup>[9]</sup>. The thawed plasma and serum samples, either fresh or frozen,
were prepared for NMR analysis by mixing 3:1 (v/v) sample and citrate buffer (pH 4.4) resulting in a target pH of 5.3, 
prepared either manually or by automated on-board mixing on the Vantera® Clinical Analyzer(LipoScience, now LabCorp, Raleigh, NC). 
The Vantera fluidics system drew 240 muL of serum and 80 muL of citrate buffer, during automated sample processing, and after mixing, 
introduced the mixture into the detection cell<sup>[9]</sup>. “The Vantera Clinical Analyzer is a fully automated high-throughput NMR platform equipped with a 
400 MHz (9.4 T) Agilent spectrometer, a 4 millimeter indirect detection probe and a fixed flow cell that was equilibrated at 47 °C via a variable temperature control module (Agilent Technologies, Santa Clara, CA)”<sup>[9]</sup>.

From the samples one-dimensional (1D) proton (<sup>1</sup>H) NMR spectra were recorded. Enhanced through T1 effects (WET) gradient sequence 
which was applied for 80.4 milliseconds (ms), the H<sub>2</sub>O resonance was attenuated using water suppression<sup>[9]</sup><sup>[10]</sup>. Total acquisition time for 
each spectrum was 5.5 minutes. The spectral width was 4496.4 Hz, steady state scans were 4, relaxation delay between scans was 5 seconds, 
direct acquisition time was 1.2 seconds, and the number of scans were 4827.


## Workflow
* * *

An overview of the python script:

![image](images/pre_process_spectra.png)

![image](images/post_processing_quantification_steps.png)

## Results
* * *

Some of the results that were produced:

Fourier transorm:

![image](images/fourier_transformation.png)

Phase correction:

![image](images/phase_correction.png)

PCA:

![image](images/pca_without_outliers.png)

Linear regression:

![image](images/linear_regression_bin_4_15.png)

## Installation
* * *

All of the components must be installed in order to run the python script. Check the website of the different python packages to 
find out how they need to be installed. 

## Requirements
* * *

| Software                            		 | Version 			    |
| -----------------------------------------  | :------------------: |
| [Python](https://www.python.org/) 		 | `3 (or later)` |


## Components
* * *

| Package                           		 | Version 			    |
| -----------------------------------------  | :------------------: |
| [Nmrglue](https://www.nmrglue.com/) <sup>[1]</sup>		 | `0.8` |
| [Numpy](https://numpy.org/) <sup>[2]</sup> | `1.18.5`	    |
| [Scipy](https://www.scipy.org/) <sup>[3]</sup>       | `1.4.1`   |
| [Pandas](https://pandas.pydata.org/) <sup>[4]</sup>   | `1.0.1`   |
| [Matplotlib](https://matplotlib.org/) <sup>[5]</sup>   | `3.1.3`              |
| [Sklearn](https://scikit-learn.org/stable/) <sup>[6]</sup> | `0.22.1`                |
| [Statsmodel](https://www.statsmodels.org/stable/index.html) <sup>[7]</sup> | `0.11.0`                |
| [Seaborn](https://seaborn.pydata.org/) <sup>[8]</sup> | `0.11.0`                |


## Credits
* * *
Graduation project of BSCs bio-informatics student at the Hanze University of Applied Sciences, Stijn Arends (stijnarends@live.nl) 
in order of the Faculty of Science and Engineering from the University of Groningen (UG). 

Special thanks to prof. dr. Peter L. Horvatovich and MSc A. Sánchez Brotons for providing guidance and support during this project. 

## License
* * *

License of the project: [license](LICENSE.md)

## References
* * *

* ***[1]***: J.J. Helmus, C.P. Jaroniec, ***Nmrglue: An open source Python package for the analysis of multidimensional NMR data***, J. Biomol. NMR 2013, 55, 355-367, http://dx.doi.org/10.1007/s10858-013-9718-x.
* ***[2]***: Charles R. Harris, K. Jarrod Millman, Stéfan J. van der Walt, Ralf Gommers, Pauli Virtanen, David Cournapeau, Eric Wieser, Julian Taylor, Sebastian Berg, Nathaniel J. Smith, Robert Kern, Matti Picus, Stephan Hoyer, Marten H. van Kerkwijk, Matthew Brett, Allan Haldane, Jaime Fernández del Río, Mark Wiebe, Pearu Peterson, Pierre Gérard-Marchant, Kevin Sheppard, Tyler Reddy, Warren Weckesser, Hameer Abbasi, Christoph Gohlke & Travis E. Oliphant. ***Array programming with NumPy***, Nature, 585, 357–362 (2020), [DOI:10.1038/s41586-020-2649-2](https://www.nature.com/articles/s41586-020-2649-2) 
* ***[3]***: Pauli Virtanen, Ralf Gommers, Travis E. Oliphant, Matt Haberland, Tyler Reddy, David Cournapeau, Evgeni Burovski, Pearu Peterson, Warren Weckesser, Jonathan Bright, Stéfan J. van der Walt, Matthew Brett, Joshua Wilson, K. Jarrod Millman, Nikolay Mayorov, Andrew R. J. Nelson, Eric Jones, Robert Kern, Eric Larson, CJ Carey, İlhan Polat, Yu Feng, Eric W. Moore, Jake VanderPlas, Denis Laxalde, Josef Perktold, Robert Cimrman, Ian Henriksen, E.A. Quintero, Charles R Harris, Anne M. Archibald, Antônio H. Ribeiro, Fabian Pedregosa, Paul van Mulbregt, and SciPy 1.0 Contributors. ***(2020) SciPy 1.0: Fundamental Algorithms for Scientific Computing in Python***. Nature Methods, 17(3), 261-272.
* ***[4]***: Wes McKinney. ***Data Structures for Statistical Computing in Python***, Proceedings of the 9th Python in Science Conference, 51-56 (2010) 
* ***[5]***: John D. Hunter. ***Matplotlib: A 2D Graphics Environment***, Computing in Science & Engineering, 9, 90-95 (2007), [DOI:10.1109/MCSE.2007.55](https://ieeexplore.ieee.org/document/4160265)
* ***[6]***: Fabian Pedregosa, Gaël Varoquaux, Alexandre Gramfort, Vincent Michel, Bertrand Thirion, Olivier Grisel, Mathieu Blondel, Peter Prettenhofer, Ron Weiss, Vincent Dubourg, Jake Vanderplas, Alexandre Passos, David Cournapeau, Matthieu Brucher, Matthieu Perrot, Édouard Duchesnay. ***Scikit-learn: Machine Learning in Python***, Journal of Machine Learning Research, 12, 2825-2830 (2011) 
* ***[7]***: Seabold, Skipper, and Josef Perktold. ***“statsmodels: Econometric and statistical modeling with python.”*** Proceedings of the 9th Python in Science Conference. 2010.
* ***[8]***: Michael L. Waskom. *** seaborn: statistical data visualization.*** Journal of Open Source Software, 6, 3021 (2021), [DOI:10.21105/joss.03021](https://doi.org/10.21105/joss.03021)
* ***[9]***: Garcia E, Wolak-Dinsmore J, Wang Z, Li XS, Bennett DW, Connelly MA, Otvos JD, Hazen SL, Jeyarajah EJ. ***NMR quantification of trimethylamine-N-oxide in human serum and plasma in the clinical laboratory setting***. Clin Biochem. 2017 Nov;50(16-17):947-955. doi: 10.1016/j.clinbiochem.2017.06.003. Epub 2017 Jun 15. PMID: 28624482; PMCID: PMC5632584.
* ***[10]***: Stephen H. Smallcombe, Steven L. Patt, Paul A. Keifer, ***WET Solvent Suppression and Its Applications to LC NMR and High-Resolution NMR Spectroscopy***,Journal of Magnetic Resonance, Series A,Volume 117, Issue 2,1995, Pages 295-303, ISSN 1064-1858, https://doi.org/10.1006/jmra.1995.0759. (https://www.sciencedirect.com/science/article/pii/S1064185885707590)