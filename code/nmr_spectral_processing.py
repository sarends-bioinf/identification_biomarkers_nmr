#!/usr/bin/env python3

# Imports
import sys
import argparse
import os
import nmrglue as ng
import numpy as np 
import pandas as pd
import scipy.signal
import scipy.integrate
import scipy.stats as stats
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
import statsmodels.api as sm
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import seaborn as sns; sns.set_theme()
import math
from pathlib import Path
from format_module import NMRPipeFormat, BrukerFormat, SparkyFormat, UniversalFormat, RNMRTKFormat, convert_format

plt.style.use('ggplot')

# Info
__author__ = "Stijn Arends"
__date__ = "15-03-2021"
__version__ = "v.0.1"

# Design patterns: https://refactoring.guru/design-patterns/catalog, https://python-patterns.guide/ 

class PreProcessData:

	def __init__(self):
		pass


	def process_data(self, dic, data):
		"""
		Process the data for a single sample.

		:paramters
		----------
		dic - dictionary
			Dictionary of Agilent/Varian parameters
		data - nd.array
			Numpy array of the data
		
		:returns
		--------
		dic_li - list
			List of the dictionaries of all the spectra from a sample
		data_arrays - nd.array
			Numpy array of the processed data
		ppm_scales - nd.array
			Numpy array of the ppm scales of the spectras
		"""

		dic_li = []
		data_li = []
		ppm_li = []

		udic = self.create_udic(dic, data)

		for i in range(data.shape[0]):
			# Convert the data into another format
			dic_conv, data_conv = self.convert_data(dic, data[i], udic)

			# Fourier transform
			dic_ft, data_ft = self.fourier_transform(dic_conv, data_conv)

			# print(f"Dictionary after fourier_transform: {dic_ft}")

			# Correct the phase of the data
			data_pc = self.correct_phase(data_ft)

			# Delete imaginaries
			dic_di, data_di = self.delete_imaginaries(dic_ft, data_pc)

			# Smooth the data
			data_smoothed = self.smooth_data(data_di)

			# Create a unit conversion object
			uc = ng.pipe.make_uc(dic_di, data_smoothed)

			# Create ppm scael
			ppm_scale = uc.ppm_scale()

			# append to lists
			dic_li.append(dic_di)
			data_li.append(data_smoothed)
			ppm_li.append(ppm_scale)

		data_arrays = np.array(data_li)
		ppm_scales = np.array(ppm_li)

		return dic_li, data_arrays, ppm_scales


	def collect_samples(self, samples_dir):
		"""
		Collect all the samples in a list that contain Agilent/Varian data,
		and return that list.

		:Parameters
		-----------
		samples_dir - string
			A path containing samples of Agilent/Varian data

		:return
		-------
		samples - list
			List containing the paths of different samples.
		"""
		try:
			samples = [os.path.join(samples_dir, sample) for sample in os.listdir(samples_dir) if sample.endswith(".fid")]
		except FileNotFoundError as e:
			sys.exit(e)
		
		return samples

	def read_varian_data(self, samples):
		"""
		Read in the varian data with the nmrglue package. 
		Create a list for both the dictionaries, which hold the varian parameter info, and the data of all the samples.

		:parameter
		----------
		samples - list
			List of the varian directories of all the samples.

		:returns
		--------
		varian_dics - list
			List of dictionaries which contain the parameter info
		varian_data - list
			List of the varian data stored in numpy arrays of all the samples
		"""
		# Create a list for the dictionaries and data arrays returned for each sample
		varian_dics, varian_arrays = map(list, zip(*map(ng.varian.read, samples)))

		return varian_dics, varian_arrays

	def create_udic(self, dic, data):
		"""
		Generate a universal dictionary based on the dic, data pair from a sample.

		:parameters
		-----------
		dic - dictionary
			Dictionary of Agilent/Varian parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		udic - dictionary
			Universal dictionary of spectral parameters 
		"""
		# Guess parameter of a universal dictionary from dic, data pair.
		udic = ng.varian.guess_udic(dic, data)

		# Fill in the parameters of the universal dictionary
		size = data.shape[1]
		sw = float(dic["procpar"]["sw"]['values'][0]) # Sweep width
		obs = float(dic["procpar"]["sfrq"]['values'][0]) # Observed frequency
		ref_freq = float(dic["procpar"]["reffrq"]['values'][0])
		ref_diff = float(dic['procpar']['reffrq1']['values'][0]) - 100.0
		car = sw/2.0 - ref_diff * ref_freq # Carrier frequency

		for n in range(udic['ndim']):
			udic[n]["sw"] = sw
			udic[n]["obs"] = obs
			udic[n]["car"] = car

			if n == 1:
				udic[n]['label'] = "H1"

		return udic

	def convert_data(self, dic, data, udic):
		"""
		Convert the Agilent/Varian format into another format.

		:Parameters
		-----------
		spectra_dir - string
			A string containing the path to a directory which contains Agilent/Varian spectra data

		:Returns
		--------
		dic_conv - Dictionary
			Dictionary of the parameters from converted format
		data_conv - ndarray
			Numpy array of the data

		:Note
		-----
		This function makes use of the format_module.py script, in order to easily
		switch between formats.
		"""

		# Initiate an instance of a class in the format module
		myFormat = NMRPipeFormat()

		# Convert the data into another format
		dic_conv, data_conv = convert_format(myFormat, dic, data, udic)

		return dic_conv, data_conv


	def zero_fill_data(self, dic, data):
		"""
		Add zeros at the end of the FID signal so that the resulting size is an 
		even multuple of the initial size. It improves the signal-to-noise ratio.

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_zero - Dictionary
			Dictionary containing the parameters 
		data_zero - ndarray
			Numpy array of the data
		"""
		# Zero fill the data
		dic_zero, data_zero = ng.pipe_proc.zf(dic, data, auto=True)

		return dic_zero, data_zero


	def fourier_transform(self, dic, data):
		"""
		Perform Complex Fourier transform on the Free induction Decay(FID) data
		with the ft() function of the pipe_proc module. 

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_ft - dictionary 
			Dictionary of the NMRpipe parameters
		data_ft - nd.array
			Numpy array of the data that has been transformed using complex Fourier
		"""
		dic_ft, data_ft = ng.pipe_proc.ft(dic, data)

		return dic_ft, data_ft


	def correct_phase(self, data, algorithm_fn = "acme"): # -> nd.array include type of return value
		"""
		Perform linear phase correction on the data.

		:parameters
		-----------
		data - nd.array
			Numpy array of the data
		algorithm_fn - str or function
			Algorithm to use for phase scoring. 
			Built in functions can be specified by one of the following strings: “acme”, “peak_minima"

		:returns
		--------
		data - nd.array
			Numpy array of the phase corrected data
		"""
		data = ng.proc_autophase.autops(data, algorithm_fn) # linear phase correction of the data

		return data


	def delete_imaginaries(self, dic, data):
		"""
		Delete the imaginaries of the data.

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_di - dictionary 
			Dictionary of the updated parameters
		data_di - nd.array
			Array of NMR data from which imaginaries have been removed
		"""

		dic_di, data_di = ng.pipe_proc.di(dic, data)

		return dic_di, data_di

	def smooth_data(self, data, window_length = 7, polyorder = 2):
		"""
		Smooth the data by applying a Savitzky-Golay filter.

		:parameters
		-----------
		data - nd.array
			Numpy array of the data
		window_length - int
			The length of the filter window (i.e., the number of coefficients). window_length
			must be a postive odd integer
		polyorder - int
			The orer of the polynominal used to fit the samples. polyorder must be less then
			window_length

		:returns
		--------
		data_smoothed - nd.array
			Numpy array of the data where the Savitzky-Golay filter has been applied.
		"""

		# smooth the data
		data_smoothed = scipy.signal.savgol_filter(data, window_length, polyorder)

		return data_smoothed


	def perform_baseline_correction(self, dic, data):
		"""
		Perform linear baseline correction on the NMR spectra data.

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_base - dictionary 
			Dictionary of the updated parameters
		data_base - nd.array
			Array of NMR data with a linear baseline correction applied.
		"""

		dic_base, data_base = ng.pipe_proc.base(dic, data)

		return dic_base, data_base


	def perform_constant_baseline_correction(self, dic, data):
		"""
		Perform constant baseline correction on the NMR spectra data.

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_base - dictionary 
			Dictionary of the updated parameters
		data_base - nd.array
			Array of NMR data with a constant baseline correction applied.

		Note:
		-----
		Still need to check if the imaginaries have been deleted from the data.
		"""
		dic_cbf, data_cbf = ng.pipe_proc.cbf(dic, data)

		return dic_cbf, data_cbf


	def perform_median_baseline_correction(self, dic, data):
		"""
		Perform median baseline correction on the NMR spectra data.

		:parameters
		-----------
		dic - dictionary
			Dictionary containing the NMRpipe parameters
		data - nd.array
			Numpy array of the data

		:returns
		--------
		dic_med - dictionary 
			Dictionary of the updated parameters
		data_med - nd.array
			Array of NMR data with a median baseline correction applied.

		Note:
		-----
		Still need to check if the imaginaries have been deleted from the data.
		"""
		dic_med, data_med = ng.pipe_proc.med(dic, data)

		return dic_med, data_med 


class PostProcessData:

	def __init__(self):
		pass


	def analyze_data(self, data, ppm_scales, results_dir):
		"""
		Locate the peaks in the spectras, calculaate the AUCs of the peaks, and calculate the sum of the peak 
		intensities. Quantify the peak information by creating a table. Write out the peak table to a csv file.

		:pararmeters
		data - nd.array
			Numpy array of the data
		ppm_scale - nd.array
			Numpy array of the ppm scale of a spectra

		:return
		-------
		df_li - list
			List of pandas dataframe which hold the peaks information for every spectra in a sample
		"""

		# Create a directory for the peak table
		peak_dir = Path(results_dir, 'peak_table')
		peak_dir.mkdir(parents = True, exist_ok = True)

		# Use ndim to check if a for loop is necessary
		# print(data.ndim)
		# print(data[0].ndim)

		# Dataframe list
		df_li = []

		# if data.ndim != 1:
		for i in range(data.shape[0]):

			# Extract peak info
			peak_data = self.collect_peak_info(data[i], ppm_scales[i])

			# Calculate AUCs
			AUCs = self.calculate_auc_peaks(peak_data['ppm_peak_widths'], peak_data['intensities_peak_widths'])

			sum_peak_inensities = self.calculate_sum_intensities_peaks(peak_data['intensities_peak_widths'])
			
			# ---------------- QUANTIFYING THE DATA ------------------
			df = self.create_dataframe(peak_data['peak_pos'], peak_data['peak_heights'], sum_peak_inensities, 
				peak_data['peak_widths'], peak_data['ppms_left_bases'], peak_data['ppms_right_bases'], AUCs)

			df_li.append(df)

			try:
				# Write the peak table out to a csv file
				df.to_csv(peak_dir / 'peak_table{}.csv'.format(i+1), sep = '\t') # Check if this work
			except PermissionError as e:
				print(e)

		return df_li

	def collect_peak_info(self, data, ppm_scale):
		"""
		Collect the peak information of the identified peaks from a sample.
		
		:parameters
		data - nd.array
			Numpy array of the data
		ppm_scale - nd.array
			Numpy array of the ppm scale of a spectra

		:returns
		--------
		peak_data - dictionary
			peak_pos - nd.array
				Numpy array of the ppm values of the peaks
			peak_heights - nd.array
				Numpy array of the peak heights
			peak_widths - nd.array
				Numpy array of the width of each peak in ppm
			ppms_left_bases - nd.array
				Numpy array of the ppm values of the left bases of each peak
			intensities_left_bases - nd.array
				Numpy array of the intensity values of the left bases of each peak
			ppms_right_bases - nd.array
				Numpy array of the ppm values of the right bases of each peak
			intensities_right_bases - nd.array
				Numpy array of the intensity values of the right bases of each peak
			ppm_peak_widths - list
				List of numpy arrays containing the ppm values from the left to right bases of a peak
			intensities_peak_widths - list
				List of numpy arrays containing the intensity values from the left to right bases of a peak
		"""
		# Find peaks
		peaks_info = self.pick_peak(data)

		# Extract the peak heights
		peak_heights = peaks_info[1]["peak_heights"]

		# Get the ppm position of the peaks
		peak_pos = ppm_scale[peaks_info[0]]

		# Get the indices of the left and right bases of the peak
		left_bases = peaks_info[1]["left_bases"]
		right_bases = peaks_info[1]["right_bases"]

		# Grab the ppm and intensity values of the left and right bases
		ppms_left_bases = ppm_scale[left_bases]
		ppms_right_bases = ppm_scale[right_bases]

		intensities_left_bases = data[left_bases]
		intensities_right_bases = data[right_bases]

		# Calculate the width of the peaks in ppm
		peak_widths = abs(ppms_left_bases - ppms_right_bases)

		# Get the ppm values from the left to right bases of each peak
		ppm_peak_widths = [ppm_scale[left_bases[j] : right_bases[j]] for j in range(left_bases.shape[0])]
		# Get the intensity values from the left to right bases of each peak
		intensities_peak_widths = [data[left_bases[k] : right_bases[k]] for k in range(left_bases.shape[0])]

		# Put the data inside a dictionary
		peak_data = dict(peak_pos = peak_pos, peak_heights = peak_heights, peak_widths = peak_widths, ppms_left_bases = ppms_left_bases,
			intensities_left_bases = intensities_left_bases, ppms_right_bases = ppms_right_bases, intensities_right_bases = intensities_right_bases, 
			ppm_peak_widths = ppm_peak_widths, intensities_peak_widths = intensities_peak_widths)

		return peak_data

	def pick_peak(self, data, threshold = 10000, width = 3, prominence = 6000):
		"""
		Grab the locations of the peaks

		:parameters
		-----------
		data - nd.array
			Numpy array of the data
		threshold - float
			Threshold for positive peaks
		width - int
			Required width of peaks in samples.
		prominence - int
			Required prominence of peaks

		:returns
		--------
		peaks_info - tuple
			peaks - nd.array
				Numpy array containing the indices of peaks in x that satisfy all given conditions.
			properties - dict
				A dictionary containing properties of the returned peaks which were calculated as intermediate results during evaluation of the specified conditions:
				* peak_heights - nd.array
					the height of each peak in x.
				* prominences - nd.array
					The calculated prominences for each peak in peaks
				* right_bases, left_bases - nd.array
					The peaks’ bases as indices in x to the left and right of each peak. The higher base of each pair is a peak’s lowest contour line.
				* widths - nd.array
					The widths for each peak in samples
				* width_heights - nd.array
					The height of the contour lines at which the widths where evaluated.
				* left_ips, right_ips - nd.array
					Interpolated positions of left and right intersection points of a 
					horizontal line at the respective evaluation height.

		""" 
		peaks_info = scipy.signal.find_peaks(data, height = threshold, width = width, prominence = prominence)

		return peaks_info

	def calculate_sum_intensities_peaks(self, intensities_peak_widths):
		"""
		Calculate the sum of the intensities from the left to the right bases of each peak.

		:paramter
		---------
		intensities_peak_widths - list
			List containing the intensity values from the left to the right bases of each peak

		:returns
		--------
		sum_intensities - nd.array
			Numpy array containing the sum of the intensities from the left to the right bases of each peak
		"""

		iterable = (sum(intensities) for intensities in intensities_peak_widths)
		sums_intensities = np.fromiter(iterable, np.float64)

		return sums_intensities


	def calculate_auc_peaks(self, ppm_peak_widths, intensities_peak_widths):
		"""
		Calculate the AUC of the identified peaks with the trapzoidal rule.

		:parameters
		-----------
		ppm_peak_widths - list
			List containing the ppm values from the left to the right bases of each peak
		intensities_peak_widths - list
			List containing the intensity values from the left to the right bases of each peak

		:returns
		--------
		auc_s = nd.array
			Numpy array of the AUCs of all the peaks from one sample
		"""
		# Calculate the AUC of each peak
		iterable = (np.trapz(np.flip(intensities, 0), np.flip(ppm, 0)) for intensities, ppm in zip(intensities_peak_widths, ppm_peak_widths))
		auc_s = np.fromiter(iterable, np.float64)

		return auc_s

	def create_dataframe(self, peak_pos, peak_heights, sum_peak_heights, peak_widths, left_bases, right_bases, AUCs):
		"""
		Create a panda dataframe of the ppm and intesities values of the peaks

		:parameters
		-----------
		peak_pos - nd.array
			Numpy array of the ppm values of the peaks that were identified
		peak_heights - nd.array
			Numpy array of the intensity values of the peak heights that were identified
		peak_width - nd.array
			The widths for each peak in samples
		left_bases - nd.array
			The peaks bases as indices in x to the left of each peak. The higher base of each pair is a peak’s lowest contour line.
		right_bases - nd.array
			The peaks bases as indices in x to the right of each peak. The higher base of each pair is a peak’s lowest contour line.
		AUCs - nd.array
			Numpy array of the AUCs of all the peaks

		:returns
		--------
		df - pandas.core.frame.DataFrame
			Pandas dataframe containing the ppm and intensities values of the identified peaks
		"""

		# Create a panda dataframe of the ppm and intensitie values
		df = pd.DataFrame({'ppm values': peak_pos, 'peak heights' : peak_heights, 'cumulative sum peak heights': sum_peak_heights ,'width(ppm)' : peak_widths, 'left_bases(ppm)' : left_bases, 'right_bases(ppm)' : right_bases, 'AUC' : AUCs}, 
			columns = ['ppm values', 'peak heights', 'cumulative sum peak heights', 'width(ppm)', 'left_bases(ppm)', 'right_bases(ppm)', 'AUC'], index = ["peak {}".format(i + 1) for i in range(peak_pos.shape[0])])

		return df

	def create_bins(self, min_ppm, max_ppm, delta, n_bins):
		"""
		Create a list of bins based on the minimum and maximum ppm value, and the 
		defined number of bins.

		:parameters
		-----------
		min_ppm - float
			Minimum ppm value across all samples
		max_ppm - float
			Maximum ppm value across all samples
		delta - float
		n_bins - int
			Number of bins

		:return
		-------
		bins - list
			List of the bin values
		"""

		bins = [min_ppm + float(delta) * i for i in range(0, n_bins)]

		return bins[::-1]

	def sum_intensity_bins(self, data_arrays, ppm_scales, delta, min_ppm, n_bins):
		"""
		Create an array of the total intensity of each bin for all the spectra across all the samples.

		:parameters
		-----------
		data_arrays - nd.array
			Numpy array of the data of all the samples
		ppm_scales - nd.array
			Numpy array of the ppm scales of all the samples
		delta - float
		min_ppm - float
			Minimum ppm value of all samples
		n_bins - int
			Number of bins

		:return
		-------
		all_intensities - np.array
			Numpy array of the total intensities of each bin for all the spectra across all the samples

		:Note
		-----
		Might want to use AUC instead of summed intensities 
		"""

		all_intensities = []

		for j in range(data_arrays.shape[0]):

			for k in range(data_arrays[0].shape[0]):

				intensities = np.zeros(n_bins)

				for i, inte in enumerate(data_arrays[j][k]):
					ppm = ppm_scales[j][k][i]
					# Locate in which bin this ppm value belongs
					index = int((ppm - min_ppm) / delta) 

					# sum the intensities (LOOK INTO THIS WHAT IS EXCATLY GOING ON?) 
					intensities[index] += inte
				all_intensities += [intensities]

		return np.array(all_intensities)

	def create_dataframe_bins(self, bins, bin_intensities, row_names):
		"""
		Create a dataframe for the bins, where rows equals samples and columns equal to the 
		different bins.

		:parameters
		-----------
		bins - nd.array
			Numpy array of the bin values
		bin_intensities - nd.array
			Numpy array of the total intensities of each bin for all the spectra across all the samples
		row_names - List
			List of the row names

		:return
		-------
		column_names - list
			List of the column names
		bin_df - pd.DataFrame
			Dataframe of the summed bin intensities for each sample.
		"""
		# Round the values of the bins
		bins_rounded = np.around(bins, 2)

		# Create a dictionary for the values of each bin
		bin_dict = dict()

		for i in range(bin_intensities.shape[1]):
			bin_val = str(bins_rounded[i])
			bin_dict[bin_val] = bin_intensities[:, i]

		# Get column names
		column_names = bin_dict.keys()

		# Create a dataframe of the bin intensities.
		bin_df = pd.DataFrame(bin_dict, columns = column_names, index = row_names)

		return list(column_names), bin_df

	def sort_dataframe(self, meta_data, sample_IDs):
		"""
		Sort the meta data dataframe based on the sample IDs of the bin dataframe.

		:parameters
		-----------
		meta_data - pd.DataFrame
			Dataframe of the metadata
		sample_IDs - list
			Lis of the sample IDs

		:returns
		--------
		meta_data_selected - pd.DataFrame
			Sorted dataframe of the selected variables
		"""
		# Turn strings to ints
		samp_IDs = [int(sample_id) for sample_id in sample_IDs]


		meta_data.SampleID = meta_data.SampleID.astype("category")
		meta_data.SampleID.cat.set_categories(samp_IDs, inplace=True)

		# Create new data frame which is sorted
		meta_data_sorted = meta_data.sort_values(["SampleID"])


		# Keep the columns that we are interested in
		meta_data_selected = meta_data_sorted[['SampleID', 'RACE', "SEX", "AGE_2B", "LENGT_2B", "WEIGH_2B"]]

		return meta_data_selected

	def combine_dataframes(self, meta_data, bin_df, n_spectra):
		"""
		Combine the meta data and bin dataframe together. Multiply the rows of the meta data df
		by the amount of spectra, in order to match the bin df.

		:parameters
		-----------
		meta_data - pd.DataFrame
			Dataframe of the metadata
		bin_df - pd.DataFrame
			Dataframe of the bin data
		n_spectra - int
			Number of spectra per sample

		:returns
		--------
		bin_df - pd.DataFrame
			The metadata and bin dataframe combined
		"""
		# Create lists
		sample_ID_li = []
		race_li = []
		sex_li = []
		age_li = []
		length_li = []
		weight_li = []


		# Multiply each row in the dataframe by the number of spectra to match the bin intensities dataframe
		for i in range(len(meta_data.index)):
			for j in range(n_spectra):
				sample_ID_li.append(meta_data.iloc[i, 0])
				race_li.append(meta_data.iloc[i, 1])
				sex_li.append(meta_data.iloc[i, 2])
				age_li.append(meta_data.iloc[i, 3])
				length_li.append(meta_data.iloc[i, 4])
				weight_li.append(meta_data.iloc[i, 5])

		try:
			bin_df.insert(0, "Sample ID", np.array(sample_ID_li))
			bin_df.insert(1, "Race", np.array(race_li))
			bin_df.insert(2, "Sex", np.array(sex_li))
			bin_df.insert(3, "Age", np.array(age_li))
			bin_df.insert(4, "Length (cm)", np.array(length_li))
			bin_df.insert(5, "Weight (kg)", np.array(weight_li))
		except ValueError as e:
			print(e)

		return bin_df


class AnalyzeData:

	def __init__(self):
		pass

	def create_pca_dataframe(self, x, combined_df):
		"""
		Perform PCA on the bin dataframe data. Use two principle components.
		Create a dataframe which includes the two principal components and the 
		sex and age variables.

		:parameters
		-----------
		x - nd.array
			Numpy array of the bin dataframe values
		combined_df - pd.DataFrame
			Dataframe of the metadata and bin data

		:return
		-------
		pc_bins_df - pd.DataFrame
			Dataframe of the two principal components and the age and sex variable.
		"""

		pca_bins = PCA(n_components=2)
		pc = pca_bins.fit_transform(x)

		pc_df = pd.DataFrame(data = pc
				, columns = ['principal component 1', 'principal component 2'],
					index = list(combined_df.index))


		# pc_bins_df = pd.concat([pc_df, bin_df[['Sex']]], axis = 1)
		pc_bins_df = pd.concat([pc_df, combined_df[['Sex', 'Age']]], axis = 1)

		return pc_bins_df

	def detect_outliers(self, pc_bins_df, threshold = 3):
		"""
		Detect outliers in the pca dataframe by calculating the z score.

		:parameters
		-----------
		"""
		# Calculate z scores for the two principal components
		z = np.abs(stats.zscore(pc_bins_df.loc[:, ['principal component 1', 'principal component 2']]))

		# get index of the outliers
		index_outliers = np.where(z > threshold)[0]

		# Get names of outliers
		sample_name_outliers = pc_bins_df.index.values[index_outliers]

		return sample_name_outliers

	def two_sample_t_test(self, sample_one, sample_two):
		t_tests = {}

		for i, column in enumerate(sample_one):
			# Extract values from bin
			bin_sample_one = sample_one.loc[:, column].values
			bin_sample_two = sample_two.loc[:, column].values

			# Check variance both groups
			var_sample_one = np.var(bin_sample_one)
			var_sample_two = np.var(bin_sample_two)

			# Get ratio
			if var_sample_one >= var_sample_two:
				ratio = var_sample_one / var_sample_two
			elif var_sample_one < var_sample_two:
				ratio = var_sample_two / var_sample_one

			# If ratio less then 4 you can assume population variance are equal
			if ratio <= 4.0:
				res = stats.ttest_ind(a=bin_sample_one, b=bin_sample_two, equal_var=True)
			else:
				res = stats.ttest_ind(a=bin_sample_one, b=bin_sample_two, equal_var=False)

			# Check if the null hypothesis can be rejected
			if res[1] >= 0.05:
				reject = False
			else:
				reject = True
			t_tests[column] = [res[0], res[1], reject]

		return t_tests

	def assess_relevancy_model(self, x, y):
		"""
		Get the summary of the fit. 
		"""
		X2 = sm.add_constant(x)
		mod = sm.OLS(y, X2)
		res = mod.fit()

		return res

	def fit_data(self, reg, x, y):
		"""
		Fit the data
		"""
		reg.fit(x, y)

		predictions = reg.predict(x)

		return predictions

	def linear_regression_all_bins(self, age, bins, result_dir):
		"""
		Perform linear regression to check if there is a relationship between age and bin intensity .

		:parameters
		-----------
		age - pd.DataFrame
			Dataframe with the age values
		bins - pd.DataFrame
			Dataframe with the bin data
		result_dir - stirng
			String of the results directory 

		:returns
		--------
		bin_significant - list
			Lis of bins with significant relationship with age
		p_vals - list
			P values
		r_squared_vals - list
			R squared values
		"""

		bin_significant = []
		p_vals = []
		r_squared_vals = []

		x = age.values.reshape(-1,1)
		reg = LinearRegression()
		for i, column in enumerate(bins):

			bin_data = bins.loc[:, column]
			y = bin_data.values.reshape(-1,1)

			# Access relevancy model
			res = self.assess_relevancy_model(age, bin_data)

			summary = res.summary()
			if res.pvalues[1] < 0.05:
				bin_significant.append(column)
				p_vals.append(res.pvalues[1])
				r_squared_vals.append(res.rsquared)

			predictions = self.fit_data(reg, x, y)

			plot_linear_regression(age, bin_data, predictions, column, ['linear regression','Young', 'Old'], result_dir)

		return bin_significant, p_vals, r_squared_vals

def plot_heatmap(bin_df, result_dir):
	"""
	Plot the log2 values of the bin dataframe in a heatmap.

	:parameters
	-----------
	bins_df - pd.DataFrame
		Dataframe of the bins
	results_dir - string
		Result directory
	"""
	# Create name of the output file
	out_file = Path(results_dir, "heatmap_bin_df.png")

	log2_bin_df = np.log2(abs(bin_df))

	fig = plt.figure(figsize=(12,4))
	ax_sns = sns.heatmap(log2_bin_df)
	ax_sns.set_yticklabels(ax_sns.get_ymajorticklabels(), fontsize = 6.5) 
	ax_sns.set_xticklabels(ax_sns.get_xmajorticklabels(), rotation = 45) 
	
	ax_sns.set_xlabel("log2 of bins(ppm)")
	ax_sns.set_ylabel("Samples")
	ax_sns.set_title("Heatmap comparing the log2 of the summed bin intensities between samples")
	fig.savefig(out_file, format = 'png')


def plot_components_explained_variance(data, result_dir):
	"""
	"Plot the cumulative explained variance ratio as a function of the number of
	principal components.

	Plot can be used to check how many principal components are needed to explain N%
	of the variance.

	:parameters
	-----------
	fata - np.array
		Numpy array of the bin data
	results_dir - string
		Result directory
	"""
	# Create name of the output file
	out_file = Path(results_dir, "pca_components_variance.png")

	pca_all = PCA().fit(data)

	fig = plt.figure(figsize=(12,4))
	ax = fig.add_subplot(1,1,1)

	ax.plot(100 * np.cumsum(pca_all.explained_variance_ratio_))
	ax.set_xlabel('number of components')
	ax.set_ylabel('cumulative explained variance (%)')
	ax.set_title('The cumulative explained variance ratio as a function of the number of components')
	fig.savefig(out_file, format = 'png')


def plot_pca(pc_bins_df, result_dir, name):
	"""
	Plot a PCA score plot for the first two principal components.
	Divide the data points into four groups: old male, young male, old female and young female.

	:parameters
	-----------
	pc_bins_df - pd.DataFrame
		PCA dataframe
	results_dir - string
		Result directory
	name - string
		Output file name
	"""
	# Create name of the output file
	out_file = Path(result_dir, name)

	fig = plt.figure(figsize = (8,8))
	ax = fig.add_subplot(1,1,1) 
	ax.set_xlabel('Principal Component 1', fontsize = 15)
	ax.set_ylabel('Principal Component 2:', fontsize = 15)
	ax.set_title('2 component PCA', fontsize = 20)

	# Targets Male = 0, Female = 1, weights < 60, weight >= 60
	targets = [[0, 60], [0, 60], [1, 60], [1, 60]]
	colors = ['r', 'g', 'y', 'b']

	for i, (target, color) in enumerate(zip(targets, colors)):
		if i % 2 ==0:
			indices = (pc_bins_df['Sex'] == target[0]) & (pc_bins_df['Age'] < target[1])
			# print(f"Items: {i}, target = {target}, color = {color}")
		elif i % 2 != 0:
			indices = (pc_bins_df['Sex'] == target[0]) & (pc_bins_df['Age'] >= target[1])
			# print(f"Items !=: {i}, target = {target}, color = {color}")
		ax.scatter(pc_bins_df.loc[indices, 'principal component 1']
			, pc_bins_df.loc[indices, 'principal component 2']
			, c = color
			, s = 50)

	ax.legend(['Male young', 'Male old', 'Female young', 'Female old'])
	ax.grid() 

	fig.savefig(out_file, format = 'png')

def plot_linear_regression(age, bin_data, predictions, bin_name, labels, result_dir):
	"""
	Plot a scatter plot bin and age plus a linear regression model.

	:parameters
	-----------
	age - pd.DataFrame
		Age dataframe
	bin_data - pd.DataFrame
		Dataframe of the bin data
	Prediction -
	bin_name - string
		Name of the bin
	labels - list
		Names for the legend
	results_dir - string
		Result directory
	"""
	# Create name of the output file
	out_file = Path(result_dir, "scatter_age_{}".format(bin_name))

	fig = plt.figure(figsize = (12,12))
	ax = fig.add_subplot(1,1,1) 
	ax.set_xlabel('Age (years)', fontsize = 15)
	ax.set_ylabel('Intensity (a.u.)', fontsize = 15)
	ax.set_title('Relation bin {} and age'.format(bin_name), fontsize = 20)
	targets = [60, 60]
	colors = ['r', 'g']
	for i, (target, color) in enumerate(zip(targets, colors)):
		if i % 2 ==0:
			indices_to_keep = age < target
		elif i % 2 != 0:
			indices_to_keep = age >= target

		ax.scatter(age.loc[indices_to_keep]
			, bin_data.loc[indices_to_keep]
			, c = color
			, s = 50)

	ax.plot(
		age,
		predictions,
		c='blue',
		linewidth=2
	)

	ax.legend(labels)
	ax.grid()

	fig.savefig(out_file, format = 'png')


def plot_spectra(ppm, data, name, results_dir, single = True):
	"""
	Plot one single NMR spectra.

	:Parameters
	-----------
	ppm - nd.array 
		Numpy array containing the ppm scale
	data - nd.array
		Numpy array of the data
	name - string
		Name of the sample
	results_dir - Path
		A path that indicates the results directory of a sample
	single - boolean
		If True create one single plot, if False
		create multiple subplots
	"""

	# Create name of the output file
	out_file = Path(results_dir, "{}.png".format(os.path.splitext(name)[0]))

	if single:
		# Single plot
		fig = plt.figure(figsize=(12,4))
		ax = fig.add_subplot(1,1,1)

		for n in range(data.shape[0]):
			ax.plot(ppm, data[n].real, label = "Spectra {}".format(n + 1))

		ax.invert_xaxis()
		ax.legend()
		ax.set_title("1H-NMR spectra {}".format(name))

		# Save the figure and remove the extention of the name and replace it with png
		fig.savefig(out_file, format = 'png')

		# plt.show()

	if not single:
		#Multiple subplots
		n = math.ceil(data.shape[0] / 2)

		fig, axs = plt.subplots(figsize=(12, 6))
		for i in range(data.shape[0]):
			ax = plt.subplot(n, 2, i + 1)
			ax.plot(ppm, data[i].real, label = "Spectra {}".format(i + 1), color=plt.cm.Paired(i/10.))
			ax.invert_xaxis()
			ax.legend()

		# fig.subtitle()
		fig.suptitle('1H-NMR spectra {}'.format(name), fontsize=16)

		# Save the figure and remove the extention of the name and replace it with png
		fig.savefig(out_file, format = 'png')

		# plt.show()


def main(samples_dir, results_dir, meta_data_file):
	"""
	"""
	pre_process_data = PreProcessData()
	post_process_data = PostProcessData()
	analyze_data = AnalyzeData()

	# Load in the samples
	samples = pre_process_data.collect_samples(samples_dir)

	# Take a subset of the samples
	sub_samples = samples[0:100]

	# Basename PREVEND TMAO FID data
	basename_samples = [os.path.splitext(os.path.basename(sample))[0] for sample in sub_samples]

	# Define result directory for each sample
	result_dir_samples = [Path(results_dir, basename) for basename in basename_samples]

	# Make the directory, if it already exist nothing happends
	for result_dir in result_dir_samples:
		# Create directory for the results
		result_dir.mkdir(parents = True, exist_ok = True)

	# Read in the varian data
	varian_dics, varian_arrays = pre_process_data.read_varian_data(sub_samples)

	# ---- Pre-process data ----
	dic_li, data_li, ppm_li = map(list, zip(*map(pre_process_data.process_data, varian_dics, varian_arrays)))

	# Turn data and ppm list into np arrays
	data_arrays = np.array(data_li)
	ppm_arrays = np.array(ppm_li)

	# ---- Analyze / quantify the data ------

	# -- Peak Picking -- 

	peak_table_li = list(map(post_process_data.analyze_data, data_li, ppm_li, result_dir_samples))

	# -- Divide data into bins --
	min_ppm = np.amin(ppm_arrays)
	max_ppm = np.amax(ppm_arrays)

	delta = 0.025

	# Calculate number of bins
	n_bins = int(((max_ppm - min_ppm) / delta) + 1)

	# Create the bins
	bins = post_process_data.create_bins(min_ppm, max_ppm, delta, n_bins)
	bin_intensities = post_process_data.sum_intensity_bins(data_arrays, ppm_arrays, delta, min_ppm, n_bins)

	# Define number of spectra per sample:
	n_spectra = data_li[0].shape[0]

	#  -- Create dataframe of binned data --
	# Grab the sample ID from the file name
	sample_IDs = [basename[5:12] for basename in basename_samples]

	# Define row names
	row_names  = [ID + " " + str(i + 1) for ID in sample_IDs for i in range(n_spectra)]

	column_names, bin_df = post_process_data.create_dataframe_bins(bins, bin_intensities, row_names)

	# -- Load in metadata --
	# Load the meta data into a dataframe
	meta_data = pd.read_csv(meta_data_file, delimiter =';')

	# Grab the meta data of only the samples that are being used
	meta_data_samples = meta_data.loc[meta_data['SampleID'].isin(sample_IDs)] 

	# -- Sort the meta data data frame based on the order of the bin data frame --

	meta_data_sorted = post_process_data.sort_dataframe(meta_data_samples, sample_IDs)

	combined_df = post_process_data.combine_dataframes(meta_data_sorted, bin_df, n_spectra)

	# -- Reduce size of the bin dataframe. Grab one spectra from each sample.
	reduced_combined_df = combined_df.iloc[0:len(combined_df.index):n_spectra, :]

	# Get bin names
	features = list(column_names)
	reduced_bin_df = reduced_combined_df.loc[:, features]

	# -- HEATMAP --
	plot_heatmap(reduced_bin_df, results_dir)


	## ---- Statistical analysis ----

	# -- PCA --

	# Separating out the features
	x = reduced_bin_df.values

	# Standardizing the features
	x = StandardScaler().fit_transform(x)

	# Plot the explained variane per principal component
	plot_components_explained_variance(x, results_dir)

	pc_bins_df = analyze_data.create_pca_dataframe(x, reduced_combined_df)

	plot_pca(pc_bins_df, results_dir, "pca_outliers.png")

	# Remove outliers
	sample_name_outliers = analyze_data.detect_outliers(pc_bins_df)


	# -- Remove outliers from the dataframes --

	# PC data frame
	n_pc_bins_df = pc_bins_df.drop(sample_name_outliers)

	plot_pca(n_pc_bins_df, results_dir, "pca_without_outliers.png")

	# Normal dataframe
	n_reduced_bin_df = reduced_combined_df.drop(sample_name_outliers)


	# -- Two sample T test (male/female) --

	# Get male bin data
	male_bin_data = n_reduced_bin_df.loc[n_reduced_bin_df['Sex'] == 0]
	# Get female bin data
	female_bin_data = n_reduced_bin_df.loc[n_reduced_bin_df['Sex'] == 1]

	# Keep only the bin columns
	male_bins =  male_bin_data.loc[:, features]
	female_bins = female_bin_data.loc[:, features]

	t_tests = analyze_data.two_sample_t_test(male_bins, female_bins)

	# Create dataframe from the two sample t test results
	df_t_test = pd.DataFrame(t_tests, index = ['T statistic', 'P value', 'Reject null hypothesis'])

	# -- Linear regression bind ~ age -- 
	age = n_reduced_bin_df.loc[:, 'Age']
	bins = n_reduced_bin_df.loc[:, features]

	bin_significant, p_vals, r_squared_vals = analyze_data.linear_regression_all_bins(age, bins, results_dir)

	data = {'P value': np.round(p_vals, 4), 'R squared': np.round(r_squared_vals, 3)}

	df_linear_regression = pd.DataFrame(data, columns = ['P value', 'R squared'],
		index = bin_significant)


def addArguments():
	"""
	Define the arguments, description and epilog of the script.
	"""
	parser = argparse.ArgumentParser(prog="nmr_spectral_processing",
		description="Python script to process NMR spectra from the Agilent/Varian spectrometer",
		epilog="Contact: stijnarend@live.nl")

	# Set version
	parser.version = __version__

	# parser.add_argument('spectra_dir',
	# 	help='Directory containing spectra info')

	parser.add_argument('samples_dir',
		help='A directory containing the Agilent/Varian NMR data of multiple samples')

	parser.add_argument('results_dir',
		help='Directory to save the results.')

	parser.add_argument('meta_data',
		help='Metadata file.')

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()

	return args.samples_dir, args.results_dir, args.meta_data

if __name__ == '__main__':
	# Get commandline arguments
	samples_dir, results_dir, meta_data_file = addArguments()

	sys.exit(main(samples_dir, results_dir, meta_data_file))

