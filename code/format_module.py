#!/usr/bin/env python3

# Imports
import nmrglue as ng
import numpy as np 
import sys


# Info
__author__ = "Stijn Arends"
__date__ = "29-03-2021"
__version__ = "v.0.1"

# Link: https://stackoverflow.com/questions/24517069/python-conditionals-replacement-with-polymorphism

class NMRPipeFormat(object):

	def convert_format(self, converter):
		"""
		Convert the data from varian to NMRpipe format.

		:parameters
		-----------
		converter - ng.convert.converter
			Instance of the converter class

		:Returns
		--------
		p_dic - dictionary 
			Dictionary containing the NMRpipe parameters
		p_data - ndarray
			Numpy array of the data
		"""

		# Create data in NMRpipe format
		p_dic, p_data = converter.to_pipe()

		return p_dic, p_data


class BrukerFormat(object):

	def convert_format(self, converter):
		"""
		Convert the data from varian to bruker format.

		:parameters
		-----------
		converter - ng.convert.converter
			Instance of the converter class

		:Returns
		--------
		b_dic - dictionary 
			Dictionary containing the bruker parameters
		b_data - ndarray
			Numpy array of the data
		"""

		# Create data in bruker format
		b_dic, b_data = converter.to_bruker()

		return b_dic, b_data


class SparkyFormat(object):

	def convert_format(self, converter):
		"""
		Convert the data from varian to sparky format.

		:parameters
		-----------
		converter - ng.convert.converter
			Instance of the converter class

		:Returns
		--------
		s_dic - dictionary 
			Dictionary containing the sparky parameters
		s_data - ndarray
			Numpy array of the data
		"""
		s_dic, s_data = converter.to_sparky()

		return s_dic, s_data


class UniversalFormat(object):

	def convert_format(self, converter):
		"""
		Convert the data from varian to universal format.

		:parameters
		-----------
		converter - ng.convert.converter
			Instance of the converter class

		:Returns
		--------
		u_dic - dictionary 
			Dictionary containing the universal parameters
		u_data - ndarray
			Numpy array of the data
		"""

		u_dic, u_data = converter.to_universal()

		return u_dic, u_data


class RNMRTKFormat(object):

	def convert_format(self, converter):
		"""
		Convert the data from varian to Rowland NMR Toolkit (RNMRTK) format.

		:parameters
		-----------
		converter - ng.convert.converter
			Instance of the converter class

		:Returns
		--------
		r_dic - dictionary 
			Dictionary containing the Rowland NMR Toolkit (RNMRTK) parameters
		r_data - ndarray
			Numpy array of the data
		"""
		r_dic, r_data = converter.to_rnmrtk(agilent_compatible = True)

		return r_dic, r_data


def convert_format(obj, dic, data, udic):
	"""
	Convert the data from varian to NMRpipe format.

	Look for a design pattern to list all the formats

	:parameters
	-----------
	obj - Class object
		Class object of one of the formats
	dic - Dictionary
		Dictionary containing Varian parameters
	data - ndarray
		Numpy array of the data
	udic - Dictionary
		Universal dictionary

	:Returns
	--------
	dic_conv - dictionary 
		Dictionary containing the parameters of the specified format.
	data_conv - ndarray
		Numpy array of the of the converted data 
	"""

	# Create convertion object
	converter = ng.convert.converter()

	converter.from_varian(dic, data, udic)

	dic_conv, data_conv = obj.convert_format(converter)

	return dic_conv, data_conv

